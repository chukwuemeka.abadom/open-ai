# Openai Api
# ChatGPT API used with Java & Spring

## How to get it running
* Clone this GIT project.
* Make sure it is a Maven project and Maven is executed to load dependencies.
* Create an Account at https://openai.com & log in
* Create API key at https://beta.openai.com/account/api-keys
* Store the key in application.properties file in cloned project.
* Start it as Spring Boot application.
* For chatting with ChatGPT: http://localhost:8080/
* For drawing images with DALL-E: http://localhost:8080/image

