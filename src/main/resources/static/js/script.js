const chatHeading = document.getElementById('chat-heading');
const messageInput = document.getElementById("message-input");
const chatBox = document.getElementById("chat-container");
const sendButton = document.getElementById("send-button");
const loader = document.getElementById("loader");
const scrollToBottomButton = document.getElementById("scroll-to-bottom");

// Initialize message array
const messages = [];
let isGeneratingResponse = false;

async function sendMessage() {
    chatHeading.style.display = 'none';
    if (isGeneratingResponse) {
        return;
    }

    const userMessage = messageInput.value;

    if (userMessage === "") {
        return;
    }

    // Hide send button and show loader
    sendButton.style.display = "none";
    loader.style.display = "inline-block";

    // Add message to array
    messages.push({ role: "user", content: userMessage });
    messageInput.value = "";
    messageInput.style.height = "65px";

    const chatMessage = document.createElement("div");
    chatMessage.classList.add("chat-message");
    const andiIcon = document.createElement("img");
    andiIcon.src = "/img/andi-icon.svg";
    andiIcon.classList.add("chat-icon");
    chatMessage.appendChild(andiIcon.cloneNode());

    const andiText = document.createElement("p");
    andiText.innerText = userMessage;
    chatMessage.appendChild(andiText);
    chatBox.appendChild(chatMessage);

    // Set response flag to true
    isGeneratingResponse = true;

    const responseMessage = document.createElement("div");
    responseMessage.classList.add("chat-message");
    const lunaIcon = document.createElement("img");
    lunaIcon.src = "/img/luna-icon.svg";
    lunaIcon.classList.add("chat-icon");
    responseMessage.appendChild(lunaIcon.cloneNode());

    const responseText = document.createElement("p");
    responseMessage.appendChild(responseText);

    const cursor = document.createElement("span");
    cursor.classList.add("cursor");
    responseText.appendChild(cursor);

    chatBox.appendChild(responseMessage);

    responseText.innerText = await generateResponse(messages);

    // Set response flag to false
    isGeneratingResponse = false;

    // Hide loader and show send button
    loader.style.display = "none";
    sendButton.style.display = "block";
}

messageInput.addEventListener("keyup", async function (event) {
    if (event.key === "Enter" && !event.shiftKey) {
        await sendMessage();
    }
});

sendButton.addEventListener("click", async function (event) {
    await sendMessage();
});

async function generateResponse(conversation) {
    try {
        const requestOptions = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(conversation)
        };

        // Make an API call to get a response to the user's message
        const response = await fetch('/chat', requestOptions);

        if (response.ok) {
            const responseData = await response.text();

            // Add result from API to messages array
            messages.push({ role: "assistant", content: responseData });

            return responseData;
        } else {
            // If the response is not successful, throw an error with the status text
            throw new Error(response.statusText);
        }
    } catch (error) {
        // If an error occurs, log the error and return a default error message
        console.error(error);
        return "I'm sorry, an error occurred while processing your message.";
    }
}

function toggleScrollToBottomButton() {
    const isScrolledToBottom = chatBox.scrollHeight - chatBox.clientHeight <= chatBox.scrollTop + 1;

    if (isScrolledToBottom) {
        scrollToBottomButton.style.display = "none";
    } else {
        scrollToBottomButton.style.display = "block";
    }
}

function scrollToBottom() {
    chatBox.scrollTop = chatBox.scrollHeight;
}

function autoResize() {
    messageInput.style.height = "65px";
    const contentHeight = messageInput.scrollHeight;
    const minHeight = messageInput.clientHeight;

    messageInput.style.height = (contentHeight > minHeight) ? `${contentHeight}px` : `${minHeight}px`;
}

messageInput.addEventListener("input", autoResize);
scrollToBottomButton.addEventListener("click", scrollToBottom);
chatBox.addEventListener("scroll", toggleScrollToBottomButton);
chatBox.addEventListener("DOMNodeInserted", scrollToBottom);

// On page load, scroll to the bottom
scrollToBottom();