package digital.and.openai.api.dalle;

public record GenerateImageRequest(String prompt, int n,
								   String size, String response_format) {
	
	public static GenerateImageRequest generateImageRequest(String prompt) {
		return new GenerateImageRequest(prompt, 1, "1024x1024", "url");
	}
	
}
