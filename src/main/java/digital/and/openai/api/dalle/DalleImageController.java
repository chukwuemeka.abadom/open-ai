package digital.and.openai.api.dalle;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import com.fasterxml.jackson.databind.ObjectMapper;

import digital.and.openai.service.FormMessageInput;
import digital.and.openai.service.OpenAiService;
import digital.and.openai.service.OpenAiApi;

@Controller
public class DalleImageController {
	
	public static final String IMAGE_PAGE = "image";
	
	@Autowired private ObjectMapper jsonMapper;
	@Autowired private OpenAiService service;
	
	private String drawImageWithDallE(String prompt) throws Exception {
		var generateImageRequest = GenerateImageRequest.generateImageRequest(prompt);
		var postBodyJson = jsonMapper.writeValueAsString(generateImageRequest);
		var responseBody = service.postToOpenAiApi(postBodyJson, OpenAiApi.DALL_E);
		var generateImageResponse = jsonMapper.readValue(responseBody, GenerateImageResponse.class);
		return generateImageResponse.firstImageUrl().orElseThrow();
	}
	
	@GetMapping(IMAGE_PAGE)
	public String paintImage() {
		return IMAGE_PAGE;
	}
	
	@PostMapping(IMAGE_PAGE)
	public String drawImage(Model model, FormMessageInput imageRequest) throws Exception {
		model.addAttribute("request", imageRequest.prompt());
		model.addAttribute("imageUri", drawImageWithDallE(imageRequest.prompt()));
		return IMAGE_PAGE;
	}

}
