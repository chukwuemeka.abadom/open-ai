package digital.and.openai.api.chatgpt;

import java.util.List;

public record ChatCompletionRequest(
		String model,
		List<ChatMessage> messages,
		double temperature,
		int max_tokens) {
	
	public static ChatCompletionRequest createChatRequest(List<ChatMessage>  prompt) {
		return new ChatCompletionRequest("gpt-3.5-turbo", prompt , 0.7, 1000);
	}
}
