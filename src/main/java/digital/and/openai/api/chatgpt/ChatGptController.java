package digital.and.openai.api.chatgpt;

import com.fasterxml.jackson.databind.ObjectMapper;
import digital.and.openai.service.OpenAiApi;
import digital.and.openai.service.OpenAiService;
import jakarta.servlet.http.HttpServletRequest;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

@Controller
public class ChatGptController {

	// This creates the "CHATGPT" level to log our requests and responses.
	final Level CHATGPT = Level.forName("CHATGPT", 350);
	Logger logger = LogManager.getLogger(ChatGptController.class.getName());
	private static final String MAIN_PAGE = "index";
	private static final String CHAT_PAGE = "chat";

	@Autowired private ObjectMapper jsonMapper;
	@Autowired private OpenAiService service;
	
	private String chatResponse(List<ChatMessage> conversations) throws Exception {
		var chatCompletionRequest = ChatCompletionRequest.createChatRequest(conversations);
		var postBodyJson = jsonMapper.writeValueAsString(chatCompletionRequest);
		var responseBody = service.postToOpenAiApi(postBodyJson, OpenAiApi.GPT_3);
		var chatCompletionResponse = jsonMapper.readValue(responseBody, ChatCompletionResponse.class);
		return chatCompletionResponse.firstContent().orElseThrow();
	}
	
	@GetMapping(path = "/")
	public String index() {
		return MAIN_PAGE;
	}

	@GetMapping(CHAT_PAGE)
	public String chat() {
		return CHAT_PAGE;
	}

	@PostMapping(path = "/chat")
	public ResponseEntity<String> conversation(@RequestBody List<ChatMessage> conversations,
											   HttpServletRequest request) throws Exception {

		//Get clients ipAddress
		String ipAddress = request.getRemoteAddr();

		//Retrieve only the contents from the chat messages
		List<String> previousConversations = conversations.stream().map(ChatMessage::content).toList();

		logger.log(CHATGPT,  "[" + ipAddress + "]Request sent= " + previousConversations);
		String chatResponse = chatResponse(conversations);
		logger.log(CHATGPT,  "Response received: " + chatResponse);
		return ResponseEntity.ok(chatResponse(conversations));
	}

}
