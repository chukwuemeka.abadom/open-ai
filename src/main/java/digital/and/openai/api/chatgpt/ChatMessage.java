package digital.and.openai.api.chatgpt;

record ChatMessage(
        String role,
        String  content
) {
    /**
     * see {@link ChatMessage} documentation.
     */
    public enum ChatMessageRole {
        SYSTEM,
        USER,
        ASSISTANT
    }
}
