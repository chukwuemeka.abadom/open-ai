package digital.and.openai.api.chatgpt;

import java.util.List;
import java.util.Optional;

public record ChatCompletionResponse(
		Usage usage,
		List<Choice> choices) {
	
	public Optional<String> firstContent() {
		if (choices == null || choices.isEmpty())
			return Optional.empty();
		return Optional.of(choices.get(0).message.content());
	}
	
	record Usage(
			int total_tokens,
			int prompt_tokens,
			int completion_tokens) {}
	
	record Choice(ChatMessage message) {}
}
