package digital.and.openai.service;

public enum OpenAiApi {
    DALL_E, GPT_3;
}