package digital.and.openai.service;

public record FormMessageInput(String prompt) {}
